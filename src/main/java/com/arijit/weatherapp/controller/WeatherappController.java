package com.arijit.weatherapp.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.arijit.weatherapp.util.ExtractPass;

@RestController
@RequestMapping("/api/internal")
public class WeatherappController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ExtractPass extractPass;

    private static final String URL = "http://dataservice.accuweather.com/locations/v1/regions?apikey=";

    @GetMapping("/v1/regionList")
    public ResponseEntity<Object> regionList() throws IOException {
        String apiKey = extractPass.getString("coreweather_api_key");
        return new ResponseEntity<>(restTemplate.getForObject(URL + apiKey, Object.class), HttpStatus.OK);
    }
}