package com.arijit.weatherapp.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class ExtractPass {

    public String getString(String getKey) throws IOException {
        String pass = null;
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("secrets.txt")) {
            if (inputStream != null) {
                Properties properties = new Properties();
                properties.load(inputStream);
                pass = properties.getProperty(getKey);
            }
        } catch (IOException e) {
            throw new IOException(e);
        }
        return pass;
    }
}