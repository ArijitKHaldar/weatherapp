package com.arijit.weatherapp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.jobs.annotations.Recurring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.arijit.weatherapp.controller.WeatherappController;

@Component
public class ScheduleWeatherJob {

    @Autowired
    private WeatherappController weatherappController;

    @Autowired
    private WriteToExcel writeToExcel;

    @Recurring(id = "get-regionlist-job", cron = "*/60 * * * * *")
    @Job(name = "Get RegionList Job")
    public void enqueueJob() throws IOException {
        Object weatherData = weatherappController.regionList().getBody();
        writeToExcel.writeDataToCSV(weatherData);
    }
}