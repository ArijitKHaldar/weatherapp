package com.arijit.weatherapp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class WriteToExcel {
    public void writeDataToCSV(Object data) throws IOException, NullPointerException {
        Workbook workbook;
        File excelFile = new File("./data/WeatherData.xlsx");
        if (excelFile.exists()) {
            try (FileInputStream inputStream = new FileInputStream(excelFile)) {
                workbook = WorkbookFactory.create(inputStream);
            }
        } else {
            workbook = new XSSFWorkbook();
        }

        Sheet sheet = workbook.getSheet("RegionList");
        if (sheet == null) {
            sheet = workbook.createSheet("RegionList");
        }

        int rowCount = sheet.getLastRowNum() + 1;

        // If no rows exist, create headers for each parameter
        if (rowCount == 0) {
            Row headerRow = sheet.createRow(rowCount++);
            Object firstEntry = ((List<?>) data).get(0);
            if (firstEntry instanceof Map) {
                int cellCount = 0;
                for (Object key : ((Map<?, ?>) firstEntry).keySet()) {
                    Cell cell = headerRow.createCell(cellCount++);
                    cell.setCellValue(key.toString());

                    // Apply cell styles
                    CellStyle style = workbook.createCellStyle();
                    Font font = workbook.createFont();
                    font.setBold(true);
                    style.setFont(font);
                    style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cell.setCellStyle(style);
                }
            }
        }

        // Write data rows
        for (Object entry : (Iterable<?>) data) {
            Row row = sheet.createRow(rowCount++);
            if (entry instanceof Map) {
                int cellCount = 0;
                for (Object value : ((Map<?, ?>) entry).values()) {
                    Cell cell = row.createCell(cellCount++);
                    cell.setCellValue(value.toString());
                }
            }
        }

        try (FileOutputStream outputStream = new FileOutputStream("./data/WeatherData.xlsx")) {
            workbook.write(outputStream);
        }
        workbook.close();
    }
}
